import data from './data.json'

const questions = document.getElementById('questions')

function renderItems() {
  let html = ''

  data.forEach(question => {
    html += `
      <div class="questions__item">
        <div class="question__heading">
          <span>${question.title}</span>
          <img src="/icon-arrow-down.svg" alt="Arrow icon" />
        </div>
        <div class="question__body">${question.body}</div>
      </div>
    `
  })

  questions.innerHTML = html
}

function hideItem(item) {
  item.classList.remove('active')
  item.querySelector('.question__body').style.maxHeight = ''
}

function handleItemClick(event) {
  const item = event.target.closest('.questions__item')
  const itemBody = item.querySelector('.question__body')
  const isActive = item.classList.contains('active')

  // Hiding all elements
  document.querySelectorAll('.questions__item').forEach(item => hideItem(item))

  // If was active, just don't let open again
  if (isActive) return
  item.classList.add('active')
  itemBody.style.maxHeight = `${itemBody.scrollHeight}px`
}

renderItems()
document.querySelectorAll('.question__heading').forEach(item => item.addEventListener('click', handleItemClick))
